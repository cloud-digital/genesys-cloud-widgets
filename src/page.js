window._genesys = {
    widgets: {
        webchat: {
            chatButton: {
                enabled: false,
            },
        },
        onReady: function () {
            document.getElementById('chat_button').disabled = false;
        },
    },
};

// Load widgets-core after configuration object
CXBus.loadPlugin('widgets-core');

// Create a plugin (or reuse your own if you have one already)
var oPlugin = CXBus.registerPlugin('myPlugin');

// Optionally, use before function to intercept 'WebChat.open' command
// and manipulate the input 'options' object before execution continues
oPlugin.before('WebChat.open', function (options) {
    // We'll append an additional form field to the one already added
    // by 'open' command in myChatButton() function triggered by button
    if (options.formJSON) options.formJSON['inputs'].push({ name: 'email', label: 'Email', placeholder: 'Required' });

    // Still need to return the 'options' object to continue execution
    return options;
});

// this function simulates 'open' command originated externally, like a button in this case
function myChatButton() {
    CXBus.command('WebChat.open', {
        formJSON: {
            inputs: [{ name: 'firstname', placeholder: 'Required', label: 'First Name' }],
        },
    });
}
